import fs from "fs";

function copyFolder(src: string, dest: string) {
  if (!fs.existsSync(src)) {
    return;
  }
  fs.mkdirSync(dest);
  fs.cpSync(src, dest, { recursive: true });
}

copyFolder("./allure-report/history/", "./test-results/allure-results/history/");
